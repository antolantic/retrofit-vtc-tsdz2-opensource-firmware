# Retrofit VTC TSDZ2 Opensource Firmware

Retrofit d'un VTC en VAE avec le moteur Tonscheng (TSDZ2)

Description complète dans le [Wiki](https://gitlab.com/antolantic/retrofit-vtc-tsdz2-opensource-firmware/-/wikis/home)

![VTC](canal.jpg)
![VTC](run.png)